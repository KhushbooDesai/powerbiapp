package com.example.a90348353.powerbiapplication.Activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a90348353.powerbiapplication.Adapter.PowerBiReportAdapter;
import com.example.a90348353.powerbiapplication.Adapter.SubFolderAdapter;
import com.example.a90348353.powerbiapplication.Helper.RetrofitAPIClient;
import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.Interface.Api;
import com.example.a90348353.powerbiapplication.Model.Report;
import com.example.a90348353.powerbiapplication.Model.ReportParam;
import com.example.a90348353.powerbiapplication.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportView extends AppCompatActivity implements View.OnClickListener {
    RecyclerView rv_powerbiReport,rv_Function,rv_ssrsReport;
    LinearLayoutManager linearLayoutManager, linearLayoutManager2;
    PowerBiReportAdapter powerBiReportAdapter;
    SubFolderAdapter subFolderAdapter;
    String userPsno;
    SharedPref sharedPref;
    ArrayList<ReportParam> powerBiArrayList,subFolderArrayList,ssrsArrayList;
    GridLayoutManager gridLayoutManager;
    TextView txtFunction,txtpowerBi,txtSSRS,txtfn;
    ImageView img_logout;

    private String functionId;
    ProgressDialog dialog;
    Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_view);

        sharedPref = new SharedPref(ReportView.this);
        initView();
        initViewAction();
        initViewListener();
    }

    private void initView() {
        rv_powerbiReport = findViewById(R.id.rv_powerbiReport);
        rv_powerbiReport.setNestedScrollingEnabled(false);
        rv_Function = findViewById(R.id.rv_Function);
        rv_Function.setNestedScrollingEnabled(false);
        rv_ssrsReport = findViewById(R.id.rv_ssrsReport);
        rv_ssrsReport.setNestedScrollingEnabled(false);

        txtFunction = findViewById(R.id.txtFunction);
        txtpowerBi = findViewById(R.id.txtpowerBi);
        txtSSRS = findViewById(R.id.txtSSRS);
         txtfn=findViewById(R.id.showfn);
         rv_Function.setOnClickListener(this);
         rv_powerbiReport.setOnClickListener(this);
        img_logout = findViewById(R.id.img_logout);

        linearLayoutManager = new LinearLayoutManager(ReportView.this, LinearLayoutManager.VERTICAL, false);
        rv_powerbiReport.setLayoutManager(linearLayoutManager);

        gridLayoutManager = new GridLayoutManager(ReportView.this,2,LinearLayoutManager.VERTICAL,false);
        rv_Function.setLayoutManager(gridLayoutManager);

        linearLayoutManager2 = new LinearLayoutManager(ReportView.this, LinearLayoutManager.VERTICAL, false);
        rv_ssrsReport.setLayoutManager(linearLayoutManager2);


        powerBiArrayList = new ArrayList();
        subFolderArrayList = new ArrayList();
        ssrsArrayList = new ArrayList();
    }

    private void initViewAction()
    {
        Intent fnintent = getIntent();


        if(getIntent().hasExtra("functionnm"))
        {
            String fnm = fnintent.getStringExtra("functionnm");
            txtfn.setText(fnm);
        }
        else if(getIntent().hasExtra("reportnm"))
        {
            String fnm = fnintent.getStringExtra("reportnm");
            txtfn.setText(fnm);
        }
        else {
            txtfn.setText("");
        }

        userPsno = sharedPref.getString("userPsno");
        dialog = new ProgressDialog(ReportView.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading..");
        dialog.show();

        if(getIntent().hasExtra("functionId"))
        {
            Bundle param=getIntent().getExtras();
            functionId = param.getString("functionId");
        }

        api = RetrofitAPIClient.getClient().create(Api.class);

        try {
            Call<Report> callReport = api.GetReportByFunction(userPsno, functionId);
            callReport.enqueue(new Callback<Report>() {
                @Override
                public void onResponse(Call<Report> call, Response<Report> response) {
                    if (response.isSuccessful())
                    {
                        filter(response.body());
                        setAdapterPowerBi(powerBiArrayList);
                        setAdapterSubFolder(subFolderArrayList);
                        setAdapterSSRS(ssrsArrayList);
                        forProgressDialog();
                    }
                    else
                    {
                        Toast.makeText(ReportView.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                        forProgressDialog();
                    }
                }



                @Override
                public void onFailure(Call<Report> call, Throwable t)
                {
                    Toast.makeText(ReportView.this, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                    Log.e("Error", t.getMessage());
                    forProgressDialog();
                }

            });
        }
        catch (Exception e)
        {
            Log.e("Exception", e.getMessage());
            forProgressDialog();
        }
    }

    public void forProgressDialog()
    {
        if (dialog.isShowing())
        {
            dialog.dismiss();
        }
    }

    private void initViewListener() {
        img_logout.setOnClickListener(this);
    }

    public void setAdapterPowerBi(ArrayList<ReportParam> reportList) {



        powerBiReportAdapter = new PowerBiReportAdapter(ReportView.this, reportList);
        rv_powerbiReport.setAdapter(powerBiReportAdapter);
        forProgressDialog();

    }

    public void setAdapterSubFolder(ArrayList<ReportParam> folderList)
    {
        subFolderAdapter = new SubFolderAdapter(ReportView.this, folderList);
        rv_Function.setAdapter(subFolderAdapter);
        forProgressDialog();
    }

    public void setAdapterSSRS(ArrayList<ReportParam> folderList)
    {
        subFolderAdapter = new SubFolderAdapter(ReportView.this, folderList);
        rv_ssrsReport.setAdapter(subFolderAdapter);
        forProgressDialog();
    }

    public void filter(Report report)
    {
        for(int i = 0;i<report.getReportArrayList().size();i++)
        {
            ReportParam rp = report.getReportArrayList().get(i);
            if(rp.getType().equals("PowerBi"))
            {
                powerBiArrayList.add(rp);
            }else if(rp.getType().equals("Folder"))
            {
                subFolderArrayList.add(rp);
            }else  if(rp.getType().equals("Report"))
            {
                ssrsArrayList.add(rp);
            }
        }
        if(powerBiArrayList.size() == 0)
        {
            txtpowerBi.setVisibility(View.GONE);
        }

        if(subFolderArrayList.size() == 0)
        {
            txtFunction.setVisibility(View.GONE);
        }

        if(ssrsArrayList.size() == 0)
        {
            txtSSRS.setVisibility(View.GONE);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

            case R.id.img_logout:
                sharedPref.putBoolean("IsFirstTimeLaunch",false);
                sharedPref.putString("userName","");
                sharedPref.putString("PSNO","");
                sharedPref.putString("date","");
                sharedPref.putString("userPSNO","");
                sharedPref.putString("userPassword","");
                sharedPref.putString("Role","");

                CookieManager.getInstance().removeAllCookies(null);

                Intent intent= new Intent(this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;

        }
    }
}
