package com.example.a90348353.powerbiapplication.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.R;

public class WelcomeActivity extends AppCompatActivity {
    WelcomeActivity activity;
    SharedPref sharedPref;
    String Password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        activity = WelcomeActivity.this;
        sharedPref = new SharedPref(activity);


        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {

                //First Time User
                if(!sharedPref.getBoolean("IsFirstTimeLaunch"))
                {
                    Intent intent = new Intent(activity,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Intent intent = new Intent(activity,FunctionView.class);
                    startActivity(intent);
                    finish();
                }
            }
        },2000);
    }
}
