package com.example.a90348353.powerbiapplication.Activity;

import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.R;

import org.apache.commons.codec.binary.Base64;

import java.util.HashMap;
import java.util.Map;

public class WebReport extends AppCompatActivity {
    WebView wv_Report;
    private String ReportLink;
    String userPsno, userPassword, Role;

    SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_report);

        sharedPref = new SharedPref(WebReport.this);

        initView();
        initViewAction();
        initViewListener();
    }

    private void initView() {
        wv_Report = findViewById(R.id.wv_Report);
    }
    private void initViewAction()
    {
        Role  = sharedPref.getString("Role");
        if(Role.equals("Admin"))
        {
            userPsno = "IEMQSAD";
            userPsno = userPsno.trim().concat("@lthed.com");
            userPassword = "Abcd@123";
        }
        else
        {
            userPsno = sharedPref.getString("userPsno");
            userPsno = userPsno.trim().concat("@lthed.com");
            userPassword = sharedPref.getString("userPassword");
        }
//        userPsno = sharedPref.getString("userPsno");
//        userPsno = userPsno.trim().concat("@lthed.com");
//
//        userPassword = sharedPref.getString("userPassword");
        if(getIntent().hasExtra("ReportLink"))
        {
            Bundle param=getIntent().getExtras();
            ReportLink = param.getString("ReportLink");
        }
        wv_Report.getSettings().setJavaScriptEnabled(true);
//        wv_Report.clearCache(true);
//        wv_Report.clearFormData();
        wv_Report.setHttpAuthUsernamePassword("pbi.lthed.com","myRealm",userPsno,userPassword);
        String userpsno = userPsno;
        String authEncoded = new String(Base64.encodeBase64(userpsno.getBytes()));
        String authHeader = "Basic " +authEncoded;
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", authHeader);
        wv_Report.setWebViewClient(new myWebClient());
//        wv_Report.setWebViewClient(new myWebClient());

//        Log.e("ReportLink",ReportLink);

        wv_Report.loadUrl(ReportLink);
    }
    private void initViewListener() {
    }

    public class myWebClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
//            super.onReceivedHttpAuthRequest(view, handler, host, realm);
//            view.loadUrl(ReportLink);
            Log.e("userPsno",userPsno);
            handler.proceed(userPsno,userPassword);
        }

    }
}
