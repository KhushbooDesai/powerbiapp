package com.example.a90348353.powerbiapplication.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a90348353.powerbiapplication.Activity.FunctionView;
import com.example.a90348353.powerbiapplication.Activity.WebReport;
import com.example.a90348353.powerbiapplication.Helper.RetrofitAPIClient;
import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.Interface.Api;
import com.example.a90348353.powerbiapplication.Model.Report;
import com.example.a90348353.powerbiapplication.Model.ReportParam;
import com.example.a90348353.powerbiapplication.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CommonFragment extends Fragment {
    TextView txtName, txtLastvisted, txtFrequent1,txtFrequent2, txtFrequent3 ;
    LinearLayout lineFrequent,lineLastVisted;
    CardView cv_frequent2,cv_frequent3;
    View view;
    SharedPref sharedPref;
    String userPsno;
    Api api;

    public CommonFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_common, container, false);
        sharedPref = new SharedPref(getActivity());
        initView();
        initViewAction();
        initViewListener();
        return view;
    }
    private void initView() {
        lineFrequent = view.findViewById(R.id.lineFrequent);
        lineLastVisted = view.findViewById(R.id.lineLastVisted);
       // txtName = view.findViewById(R.id.txtName);
        txtLastvisted = view.findViewById(R.id.txtLastvisted);
        txtFrequent1 = view.findViewById(R.id.txtFrequent1);
        txtFrequent2 = view.findViewById(R.id.txtFrequent2);
        txtFrequent3=view.findViewById(R.id.txtFrequent3);
        cv_frequent2 = view.findViewById(R.id.cv_frequent2);
        cv_frequent3 = view.findViewById(R.id.cv_frequent3);
       // txtName.setSelected(true);
        txtFrequent1.setSelected(true);
        txtFrequent2.setSelected(true);
        txtFrequent3.setSelected(true);
        txtLastvisted.setSelected(true);


    }
    private void initViewAction() {

      //  txtName.setText(sharedPref.getString("userName"));
    }
    private void initViewListener()
    {

    }

    @Override
    public void onResume() {
        super.onResume();
        userPsno = sharedPref.getString("userPsno");
        api = RetrofitAPIClient.getClient().create(Api.class);

        Call<Report> callFrequent = api.Frequent(userPsno);
        callFrequent.enqueue(new Callback<Report>()
        {

            @Override
            public void onResponse(Call<Report> call, Response<Report> response)
            {
                Log.e("FunctionView","onResponse of Frequent");
                if (response.isSuccessful())
                {
                    Log.e("FunctionView","Response is Successful");

                    ArrayList<ReportParam> reportParamArrayList = response.body().getReportArrayList();

                    Log.e("FunctionView","Size of reportParamArrayList"+reportParamArrayList.size());

                    switch (reportParamArrayList.size())
                    {
                        case 1:
                            cv_frequent2.setVisibility(View.GONE);
                            cv_frequent3.setVisibility(View.GONE);
                            break;
                        case 2:
                            cv_frequent3.setVisibility(View.GONE);
                            break;
                        case 3:
                            break;
                    }

                    int i = 1;
                    if(reportParamArrayList.size() > 0)
                    {
                        for (final ReportParam reportParam : reportParamArrayList)
                        {
                            if (i == 1)
                            {
                                txtFrequent1.setText(reportParam.getReportName());

                                //final String txt1=reportParam.getReportLink();
                                txtFrequent1.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Call<Report> callReport = api.InsertReportLog(reportParam.getReportId(), userPsno);
                                        callReport.enqueue(new Callback<Report>()
                                        {
                                            @Override
                                            public void onResponse(Call<Report> call, Response<Report> response)
                                            {
                                                if (response.isSuccessful())
                                                {
                                                    Intent intent = new Intent(getActivity(), WebReport.class);
                                                    Bundle param = new Bundle();
                                                    param.putString("ReportLink", reportParam.getReportLink());
                                                    intent.putExtras(param);
                                                    startActivity(intent);
                                                } else
                                                {
                                                    Toast.makeText(getActivity(), "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                                                    //forProgressDialog();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<Report> call, Throwable t)
                                            {
                                                Toast.makeText(getActivity(), "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                                                Log.e("Error", t.getMessage());
                                                //forProgressDialog();
                                            }

                                        });
                                    }
                                });

                            }
                            else if (i == 2)
                            {
                                txtFrequent2.setText(reportParam.getReportName());

                                txtFrequent2.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Call<Report> callReport = api.InsertReportLog(reportParam.getReportId(), userPsno);
                                        callReport.enqueue(new Callback<Report>()
                                        {
                                            @Override
                                            public void onResponse(Call<Report> call, Response<Report> response)
                                            {
                                                if (response.isSuccessful())
                                                {
                                                    Intent intent = new Intent(getActivity(), WebReport.class);
                                                    Bundle param = new Bundle();
                                                    param.putString("ReportLink", reportParam.getReportLink());
                                                    intent.putExtras(param);
                                                    startActivity(intent);
                                                } else
                                                {
                                                    Toast.makeText(getActivity(), "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                                                    //forProgressDialog();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<Report> call, Throwable t)
                                            {
                                                Toast.makeText(getActivity(), "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                                                Log.e("Error", t.getMessage());
                                                //forProgressDialog();
                                            }

                                        });
                                    }
                                });

                            }
                            else if (i == 3)
                            {
                                txtFrequent3.setText(reportParam.getReportName());

                                txtFrequent3.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Call<Report> callReport = api.InsertReportLog(reportParam.getReportId(), userPsno);
                                        callReport.enqueue(new Callback<Report>()
                                        {
                                            @Override
                                            public void onResponse(Call<Report> call, Response<Report> response)
                                            {
                                                if (response.isSuccessful())
                                                {
                                                    Intent intent = new Intent(getActivity(), WebReport.class);
                                                    Bundle param = new Bundle();
                                                    param.putString("ReportLink", reportParam.getReportLink());
                                                    intent.putExtras(param);
                                                    startActivity(intent);
                                                } else
                                                {
                                                    Toast.makeText(getActivity(), "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                                                    //forProgressDialog();
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<Report> call, Throwable t)
                                            {
                                                Toast.makeText(getActivity(), "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                                                Log.e("Error", t.getMessage());
                                                //forProgressDialog();
                                            }

                                        });
                                    }
                                });
                            }
                            i++;
                        }
                    }else
                    {
                        lineFrequent.setVisibility(View.GONE);
                    }
                    //txtLastvisted.setText(response.body().getReportName());
                    // forProgressDialog();
                    //setAdapterForFrequent(response.body());
                }
                else
                {
                    Toast.makeText(getActivity(), "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.e("response", response.message().toString());
                    Log.e("response_code", String.valueOf(response.code()));
                    //forProgressDialog();
                }
            }

            @Override

            public void onFailure(Call<Report> call, Throwable t)
            {
                Log.e("FunctionView","onFailure of Frequent");
                Toast.makeText(getActivity(), "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                Log.e("Error", t.getMessage());
                // forProgressDialog();
            }
        });

        //for lsat visted
        Call<ReportParam> call = api.LastVisited(userPsno);
        call.enqueue(new Callback<ReportParam>()
        {
            @Override
            public void onResponse(Call<ReportParam> call, final Response<ReportParam> response)
            {
                if (response.isSuccessful())
                {
                    final String link = response.body().getReportLink();
                    txtLastvisted.setText(response.body().getReportName());
                    if(txtLastvisted.getText() != "")
                    {
                        txtLastvisted.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                Call<Report> callReport = api.InsertReportLog(response.body().getReportId(), userPsno);
                                callReport.enqueue(new Callback<Report>()
                                {
                                    @Override
                                    public void onResponse(Call<Report> call, Response<Report> response)
                                    {
                                        if (response.isSuccessful())
                                        {
                                            Intent intent = new Intent(getActivity(), WebReport.class);
                                            Bundle param = new Bundle();
                                            param.putString("ReportLink", link);
                                            intent.putExtras(param);
                                            startActivity(intent);
                                        } else
                                        {
                                            Toast.makeText(getActivity(), "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                                            //forProgressDialog();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Report> call, Throwable t)
                                    {
                                        Toast.makeText(getActivity(), "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                                        Log.e("Error", t.getMessage());
                                        //forProgressDialog();
                                    }

                                });
                            }
                        });
                    }else
                    {
                        lineLastVisted.setVisibility(View.GONE);
                    }
                    // forProgressDialog();
                }
                else
                {
                    Toast.makeText(getActivity(), "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                    Log.e("response", response.message().toString());
                    Log.e("response_code", String.valueOf(response.code()));
                    //forProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<ReportParam> call, Throwable t)
            {
                Toast.makeText(getActivity(), "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                Log.e("Error", t.getMessage());
                // forProgressDialog();
            }
        });
    }

}
