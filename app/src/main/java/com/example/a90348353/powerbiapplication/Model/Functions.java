package com.example.a90348353.powerbiapplication.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Functions {
    @SerializedName("Function")
    private ArrayList<FunctionParam> functionArrayList;

    public ArrayList<FunctionParam> getFunctionArrayList()
    {
        return functionArrayList;
    }

    public void setUserArrayList(ArrayList<FunctionParam> functionArrayList)
    {
        this.functionArrayList = functionArrayList;
    }

}
