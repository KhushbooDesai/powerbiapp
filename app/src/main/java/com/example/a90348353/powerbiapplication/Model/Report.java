package com.example.a90348353.powerbiapplication.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Report {
    @SerializedName("Report")
    private ArrayList<ReportParam> reportArrayList;

    public ArrayList<ReportParam> getReportArrayList()
    {
        return reportArrayList;
    }

    public void setReportArrayList(ArrayList<ReportParam> reportArrayList)
    {
        this.reportArrayList = reportArrayList;
    }
}
