package com.example.a90348353.powerbiapplication.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a90348353.powerbiapplication.Activity.ReportView;
import com.example.a90348353.powerbiapplication.Activity.WebReport;
import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.Interface.Api;
import com.example.a90348353.powerbiapplication.Model.ReportParam;
import com.example.a90348353.powerbiapplication.R;

import java.util.ArrayList;

public class SubFolderAdapter  extends RecyclerView.Adapter<SubFolderAdapter.MyViewHolder>
{
    Context context;
    ArrayList<ReportParam> subFolderArrayList;
    Api api;
    String userPsno;
    SharedPref sharedPref;
    ProgressDialog dialog;


    public SubFolderAdapter(Context context, ArrayList<ReportParam> subFolderArrayList)
    {
        this.context = context;
        this.subFolderArrayList = subFolderArrayList;
        sharedPref = new SharedPref(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_functions, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final SubFolderAdapter.MyViewHolder holder, int position) {
        final ReportParam reportParam = subFolderArrayList.get(holder.getAdapterPosition());
        holder.txtFunctionName.setText(reportParam.getReportName());

        if (!reportParam.getReportColor().startsWith("#"))
        {
            holder.cv_function_card.setCardBackgroundColor(Color.parseColor("#".concat(reportParam.getReportColor().trim())));
        }
        else
        {
            holder.cv_function_card.setCardBackgroundColor(Color.parseColor(reportParam.getReportColor()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog = new ProgressDialog(context);
                dialog.setCancelable(false);
                dialog.setMessage("Loading..");
                dialog.show();
                //userPsno = sharedPref.getString("userPsno");
                if (reportParam.getType().equalsIgnoreCase("Folder"))
                {

                    Bundle param = new Bundle();
                    Intent intent = new Intent(context, ReportView.class);
                    param.putString("functionId", reportParam.getReportId());
                    param.putString("reportnm", reportParam.getReportName());
                    intent.putExtras(param);
                    context.startActivity(intent);
                    forProgressDialog();
                }
                else
                {
                    forProgressDialog();
                    Bundle param = new Bundle();
                    Intent intent = new Intent(context, WebReport.class);
                    param.putString("ReportLink", reportParam.getReportLink());
                    intent.putExtras(param);
                    context.startActivity(intent);
                }
//                else
//                {
//                    Bundle param = new Bundle();
//                    Intent intent = new Intent(context, ReportView.class);
//                    param.putString("functionId", reportParam.getReportId());
//                    intent.putExtras(param);
//                    context.startActivity(intent);
//                    forProgressDialog();
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return subFolderArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtFunctionName;
        CardView cv_function_card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtFunctionName = itemView.findViewById(R.id.txtFunctionName);
            cv_function_card = itemView.findViewById(R.id.cv_function_card);
            txtFunctionName.setSelected(true);
        }
    }
    public void forProgressDialog()
    {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
