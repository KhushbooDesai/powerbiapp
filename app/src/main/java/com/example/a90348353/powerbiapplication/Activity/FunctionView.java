package com.example.a90348353.powerbiapplication.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.core.util.Function;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a90348353.powerbiapplication.Adapter.FunctionAdapter;
import com.example.a90348353.powerbiapplication.Helper.RetrofitAPIClient;
import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.Interface.Api;
import com.example.a90348353.powerbiapplication.Model.Functions;
import com.example.a90348353.powerbiapplication.Model.Report;
import com.example.a90348353.powerbiapplication.Model.ReportParam;
import com.example.a90348353.powerbiapplication.Model.Version;
import com.example.a90348353.powerbiapplication.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FunctionView extends AppCompatActivity implements View.OnClickListener {
    //TextView txtLastvisted, txtFrequent1,txtFrequent2, txtFrequent3 ;
    RecyclerView rv_Function;
    LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    ImageView img_logout;
//    CardView cv_frequent2,cv_frequent3;

    private FunctionAdapter functionAdapter;
   // LinearLayout lineFrequent,lineLastVisted;
    String userPsno;
    SharedPref sharedPref;
    SimpleDateFormat df;
    Date currentDate;
    String todayDate;
// public final String txt1="";

    ProgressDialog dialog;
    Api api;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_function_view);

        sharedPref = new SharedPref(FunctionView.this);

        initView();
        initViewAction();
        initViewListener();

    }
    private void initView()
    {
//        lineFrequent = findViewById(R.id.lineFrequent);
//        lineLastVisted = findViewById(R.id.lineLastVisted);
//        txtLastvisted = findViewById(R.id.txtLastvisted);
//        txtFrequent1 = findViewById(R.id.txtFrequent1);
//        //txtFrequent1.setMovementMethod(new ScrollingMovementMethod());
//        txtFrequent2 = findViewById(R.id.txtFrequent2);
//        txtFrequent3=findViewById(R.id.txtFrequent3);
        rv_Function = findViewById(R.id.rv_Function);
        rv_Function.setNestedScrollingEnabled(true);
        img_logout = findViewById(R.id.img_logout);
//        cv_frequent2 = findViewById(R.id.cv_frequent2);
//        cv_frequent3 = findViewById(R.id.cv_frequent3);

        gridLayoutManager = new GridLayoutManager(this,2,LinearLayoutManager.VERTICAL,false);
        rv_Function.setLayoutManager(gridLayoutManager);


//        txtFrequent1.setSelected(true);
//        txtFrequent2.setSelected(true);
//        txtFrequent3.setSelected(true);
//        txtLastvisted.setSelected(true);
        // set LayoutManager to RecyclerView
//        linearLayoutManager = new LinearLayoutManager(FunctionView.this,LinearLayoutManager.VERTICAL,false);
//        rv_Function.setLayoutManager(linearLayoutManager);
    }
    private void initViewAction()
    {
        df = new SimpleDateFormat("dd/MMM/yyyy");
        currentDate = Calendar.getInstance().getTime();
        todayDate = df.format(currentDate);

        if (sharedPref.getString("date").isEmpty() || sharedPref.getString("date").equalsIgnoreCase(""))
        {
            sharedPref.putString("date",todayDate);
        }
        else
        {
            PackageInfo packageInfo = null;
            try {
                packageInfo = getPackageManager().getPackageInfo(getPackageName(),0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String version = packageInfo.versionName;

            Log.e("version",version);
            Log.e("today",todayDate);
            Log.e("shreDate",sharedPref.getString("date"));

            api = RetrofitAPIClient.getClient().create(Api.class);

            Call<Version> versionCall = api.checkVersion(version);
            versionCall.enqueue(new Callback<Version>()
            {
                @Override
                public void onResponse(Call<Version> call, final Response<Version> response)
                {
                    if(response.isSuccessful())
                    {

                        Log.e("Version","Version from service :"+response.body().getResult());

                        if(!response.body().getResult().equalsIgnoreCase("1"))
                        {

                           /* if(NetworkManager.isWifiConnected(activity) || NetworkManager.isInternetConnected(activity))
                            {
                                callDeliveredMessageApi();
                            }

                            txt_userName.setText(userName.isEmpty()?"":userName);

                            tabLayout.addTab(tabLayout.newTab().setText("Tab1"));
                            tabLayout.addTab(tabLayout.newTab().setText("Tab2"));
                            tabLayout.addTab(tabLayout.newTab().setText("Tab3"));

                            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
                            viewPager.setAdapter(viewPagerAdapter);
                            tabLayout.setupWithViewPager(viewPager);*/

                            AlertDialog.Builder builder=new AlertDialog.Builder(FunctionView.this);
                            builder.setTitle("New Version of APK")
                                    .setMessage("Please Install New Version of APK")
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            finishAffinity();
                                            System.exit(0);
                                        }
                                    })
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            sharedPref.putString("date",todayDate);
                                            Intent intent = new Intent(Intent.ACTION_VIEW,
                                                    Uri.parse(response.body().getResult()));
//                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    })
                                    .setCancelable(false)
                                    .create()
                                    .show();


                        }
                        else
                        {
                           /*AlertDialog.Builder builder=new AlertDialog.Builder(activity);
                            builder.setTitle("New Version of APK")
                                    .setMessage("Please Install New Version of APK")
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            finishAffinity();
                                            System.exit(0);
                                        }
                                    })
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Intent.ACTION_VIEW,
                                                    Uri.parse(response.body().getRes()));
//                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    })
                                    .setCancelable(false)
                                    .create()
                                    .show();*/
                        }
                    }
                    else
                    {
                        Toast.makeText(FunctionView.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                        Log.e("response",response.message());
                    }
                }

                @Override
                public void onFailure(Call<Version> call, Throwable t)
                {

                }
            });
        }


        dialog = new ProgressDialog(FunctionView.this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading..");
        dialog.show();

        userPsno = sharedPref.getString("userPsno");
        api = RetrofitAPIClient.getClient().create(Api.class);

        try
        {
            //for get function
            Call<Functions> callFucntion = api.GetFunctionData(userPsno);
            callFucntion.enqueue(new Callback<Functions>()
            {
                @Override
                public void onResponse(Call<Functions> call, Response<Functions> response)
                {
                    if(response.isSuccessful())
                    {
                        setAdapter(response.body());
                        forProgressDialog();
                    }
                    else
                    {
                        Toast.makeText(FunctionView.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                        forProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<Functions> call, Throwable t)
                {
                    Toast.makeText(FunctionView.this, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                    Log.e("Error",t.getMessage());
                    forProgressDialog();
                }

            });
        }
        catch (Exception e)
        {
            Log.e("Exception",e.getMessage());
           forProgressDialog();
        }
    }

    public void forProgressDialog()
    {
        if(dialog.isShowing())
        {
            dialog.dismiss();
        }
    }

    private void initViewListener()
    {
        img_logout.setOnClickListener(this);
    }

    private void setAdapter(Functions functionList)
    {
        functionAdapter =new FunctionAdapter(this,functionList.getFunctionArrayList());
        rv_Function.setAdapter(functionAdapter);
        forProgressDialog();
//        progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_logout:
                sharedPref.putBoolean("IsFirstTimeLaunch",false);
                sharedPref.putString("userName","");
                sharedPref.putString("PSNO","");
                sharedPref.putString("date","");
                sharedPref.putString("userPSNO","");
                sharedPref.putString("userPassword","");
                sharedPref.putString("Role","");

                CookieManager.getInstance().removeAllCookies(null);

                Intent intent= new Intent(this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);


            break;

        }
    }

}
