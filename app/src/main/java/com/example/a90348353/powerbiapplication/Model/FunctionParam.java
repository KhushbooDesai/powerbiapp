package com.example.a90348353.powerbiapplication.Model;

import com.google.gson.annotations.SerializedName;

public class FunctionParam {
    @SerializedName("FunctionId")
    private String FunctionId;

    @SerializedName("FunctionName")
    private String FunctionName;

    @SerializedName("ColorCode")
    private String ColorCode;

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getFunctionId() {
        return FunctionId;
    }

    public void setFunctionId(String functionId) {
        FunctionId = functionId;
    }

    public String getFunctionName() {
        return FunctionName;
    }

    public void setFunctionName(String functionName) {
        FunctionName = functionName;
    }
}
