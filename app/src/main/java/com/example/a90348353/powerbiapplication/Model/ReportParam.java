package com.example.a90348353.powerbiapplication.Model;

import com.google.gson.annotations.SerializedName;

public class ReportParam {

    @SerializedName("ReportId")
    private String ReportId;

    @SerializedName("ReportName")
    private String ReportName;

    @SerializedName("ReportLink")
    private String ReportLink;

    @SerializedName("Type")
    private String Type;

    @SerializedName("ReportColor")
    private String ReportColor;

    public String getReportId() {
        return ReportId;
    }

    public void setReportId(String reportId) {
        ReportId = reportId;
    }

    public String getReportName() {
        return ReportName;
    }

    public void setReportName(String reportName) {
        ReportName = reportName;
    }

    public String getReportLink() {
        return ReportLink;
    }

    public void setReportLink(String reportLink) {
        ReportLink = reportLink;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getReportColor() {
        return ReportColor;
    }

    public void setReportColor(String reportColor) {
        ReportColor = reportColor;
    }


}
