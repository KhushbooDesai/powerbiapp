package com.example.a90348353.powerbiapplication.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a90348353.powerbiapplication.Activity.WebReport;
import com.example.a90348353.powerbiapplication.Helper.RetrofitAPIClient;
import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.Interface.Api;
import com.example.a90348353.powerbiapplication.Model.Report;
import com.example.a90348353.powerbiapplication.Model.ReportParam;
import com.example.a90348353.powerbiapplication.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PowerBiReportAdapter extends RecyclerView.Adapter<PowerBiReportAdapter.MyViewHolder>
{
    Context context;
    ArrayList<ReportParam> reportArrayList;
    Api api;
    String userPsno;
    SharedPref sharedPref;
    ProgressDialog dialog;


    public PowerBiReportAdapter(Context context, ArrayList<ReportParam> reportArrayList)
    {
        this.context = context;
        this.reportArrayList = reportArrayList;
        sharedPref = new SharedPref(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.row_reports, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position)
    {

        final ReportParam reportParam = reportArrayList.get(holder.getAdapterPosition());
        holder.txtReportName.setText(reportParam.getReportName());

        if (reportParam.getType().equalsIgnoreCase("PowerBi")) {
            holder.img_rightArrow.setVisibility(View.VISIBLE);
        }
//        else
//        {
//            holder.img_rightArrow.setVisibility(View.GONE);
//        }
        if (!reportParam.getReportColor().startsWith("#"))
        {
            holder.cv_report_card.setCardBackgroundColor(Color.parseColor("#".concat(reportParam.getReportColor().trim())));
        }
        else
        {
            holder.cv_report_card.setCardBackgroundColor(Color.parseColor(reportParam.getReportColor()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog = new ProgressDialog(context);
                dialog.setCancelable(false);
                dialog.setMessage("Loading..");
                dialog.show();
                userPsno = sharedPref.getString("userPsno");
                if (reportParam.getType().equalsIgnoreCase("PowerBi"))
                {
                    try
                    {
                        Log.e("reportId",reportParam.getReportId());
                        Log.e("psno",userPsno);
                        api = RetrofitAPIClient.getClient().create(Api.class);
                        Call<Report> callReport = api.InsertReportLog(reportParam.getReportId(), userPsno);
                        callReport.enqueue(new Callback<Report>()
                        {
                            @Override
                            public void onResponse(Call<Report> call, Response<Report> response)
                            {
                                if (response.isSuccessful())
                                {
                                    Intent intent = new Intent(context, WebReport.class);
                                    Bundle param = new Bundle();

                                    String s = reportParam.getReportLink().trim();
                                    param.putString("reportnm",reportParam.getReportName());
                                    param.putString("ReportLink", s);
                                    intent.putExtras(param);
                                    context.startActivity(intent);
                                    forProgressDialog();
                                } else
                                    {
                                    Toast.makeText(context, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                                    forProgressDialog();
                                }
                            }

                            @Override
                            public void onFailure(Call<Report> call, Throwable t)
                            {
                                Toast.makeText(context, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                                Log.e("Error", t.getMessage());
                                forProgressDialog();
                            }

                        });
                    } catch (Exception e) {
                        Log.e("Exception", e.getMessage());
                        forProgressDialog();
                    }
                }
//                else
//                {
//                    Bundle param = new Bundle();
//                    Intent intent = new Intent(context, ReportView.class);
//                    param.putString("functionId", reportParam.getReportId());
//                    intent.putExtras(param);
//                    context.startActivity(intent);
//                    forProgressDialog();
//                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return reportArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView txtReportName;
        CardView cv_report_card;
        ImageView img_rightArrow;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            img_rightArrow = itemView.findViewById(R.id.img_rightArrow);
            txtReportName = itemView.findViewById(R.id.txtReportName);
            cv_report_card = itemView.findViewById(R.id.cv_report_card);
            txtReportName.setSelected(true);
        }
    }

    public void forProgressDialog()
    {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
