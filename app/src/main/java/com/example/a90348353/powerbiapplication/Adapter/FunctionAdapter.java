package com.example.a90348353.powerbiapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a90348353.powerbiapplication.Activity.ReportView;
import com.example.a90348353.powerbiapplication.Model.FunctionParam;
import com.example.a90348353.powerbiapplication.R;

import java.util.ArrayList;

public class FunctionAdapter extends RecyclerView.Adapter<FunctionAdapter.MyViewHolder> {
    Context context;
    ArrayList<FunctionParam> functionArrayList;
//    private onCardClick onCardClick;

    public FunctionAdapter(Context context, ArrayList<FunctionParam> functionsArrayList)
    {
        this.context = context;
        this.functionArrayList =functionsArrayList;
//        this.onCardClick = onCardClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_functions,viewGroup,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
//        return null;

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position)
    {

        final FunctionParam function = functionArrayList.get(holder.getAdapterPosition());
        holder.txtFunctionName.setText(function.getFunctionName());
        if (!function.getColorCode().startsWith("#"))
        {
            holder.cv_function_card.setCardBackgroundColor(Color.parseColor("#".concat(function.getColorCode().trim())));
        }
        else
        {
            holder.cv_function_card.setCardBackgroundColor(Color.parseColor(function.getColorCode()));
        }
//        holder.functionId = function.getFunctionId();

        /*holder.img_rightArrow.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(context,ReportView.class);
                Bundle param=new Bundle();
                param.putString("functionId",function.getFunctionId());
                intent.putExtras(param);
                context.startActivity(intent);
            }
        });*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,ReportView.class);
                Bundle param=new Bundle();
                param.putString("functionId",function.getFunctionId());
                param.putString("functionnm",function.getFunctionName());
                intent.putExtras(param);
                context.startActivity(intent);

            }
        });



    }

    @Override
    public int getItemCount() {
        return functionArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtFunctionName;
        CardView cv_function_card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtFunctionName = itemView.findViewById(R.id.txtFunctionName);
            cv_function_card = itemView.findViewById(R.id.cv_function_card);
        }
    }
    /*public class MyViewHolder extends RecyclerView.ViewHolder
    {
        CircleImageView img_userImg;
        TextView txt_userName,txt_user_psno,txt_designation,txt_department;
        ImageView img_fav;
        CardView cd_chat_card;

        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            img_userImg = itemView.findViewById(R.id.img_userImg);
            txt_userName = itemView.findViewById(R.id.txt_userName);
            txt_user_psno = itemView.findViewById(R.id.txt_user_psno);
            img_fav = itemView.findViewById(R.id.img_fav);
            cd_chat_card = itemView.findViewById(R.id.cd_chat_card);
            txt_department = itemView.findViewById(R.id.txt_department);
            txt_designation = itemView.findViewById(R.id.txt_designation);
        }
    }*/
    /*public interface onCardClick
    {
        public void onClickofCard(int position,User user);
    }*/
}
