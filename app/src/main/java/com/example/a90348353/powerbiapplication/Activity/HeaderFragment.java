package com.example.a90348353.powerbiapplication.Activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a90348353.powerbiapplication.Activity.FunctionView;
import com.example.a90348353.powerbiapplication.Activity.WebReport;
import com.example.a90348353.powerbiapplication.Helper.RetrofitAPIClient;
import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.Interface.Api;
import com.example.a90348353.powerbiapplication.Model.Report;
import com.example.a90348353.powerbiapplication.Model.ReportParam;
import com.example.a90348353.powerbiapplication.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HeaderFragment extends Fragment {
    TextView txtName, txtLastvisted, txtFrequent1,txtFrequent2, txtFrequent3 ;
    LinearLayout lineFrequent,lineLastVisted;
    CardView cv_frequent2,cv_frequent3;
    View view;
    SharedPref sharedPref;
    String userPsno;
    Api api;

    public  HeaderFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_header, container, false);
        sharedPref = new SharedPref(getActivity());
        initView();
        initViewAction();
        initViewListener();
        return view;
    }
    private void initView() {
        txtName = view.findViewById(R.id.txtName);
        txtName.setSelected(true);

    }
    private void initViewAction() {

        txtName.setText(sharedPref.getString("userName"));
    }
    private void initViewListener()
    {

    }

    @Override
    public void onResume() {
        super.onResume();
        userPsno = sharedPref.getString("userPsno");
    }
}
