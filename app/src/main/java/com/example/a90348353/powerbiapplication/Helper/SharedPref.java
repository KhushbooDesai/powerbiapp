package com.example.a90348353.powerbiapplication.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPref
{
    SharedPreferences preferences;

    public SharedPref(Context appContext)
    {
        preferences = PreferenceManager.getDefaultSharedPreferences(appContext);
    }

    public void putString(String key, String value)
    {
        checkForNullKey(key); checkForNullValue(value);
        preferences.edit().putString(key, value).apply();
    }

    public String getString(String key)
    {
        return preferences.getString(key, "");
    }

    public void checkForNullKey(String key)
    {
        if (key == null){
            throw new NullPointerException();
        }
    }

    public void checkForNullValue(String value)
    {
        if (value == null){
            throw new NullPointerException();
        }
    }

    public boolean getBoolean(String key) {
        return preferences.getBoolean(key, false);
    }

    public void putBoolean(String key, boolean value) {
        checkForNullKey(key);
        preferences.edit().putBoolean(key, value).apply();
    }
}
