package com.example.a90348353.powerbiapplication.Helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkManager
{
    private static ConnectivityManager connectivityManager;
    private static NetworkInfo networkInfo;

    public NetworkManager(Context context)
    {
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connectivityManager.getActiveNetworkInfo();
    }

    public static boolean checkWifiConnected()
    {
        if(networkInfo!=null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean isWifiConnected(Context context)
    {
        // Create object for ConnectivityManager class which returns network
        // related info
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // If connectivity object is not null
        if (connectivity != null)
        {
            // Get network info - WIFI internet access
            NetworkInfo info = connectivity
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                // Look for whether device is currently connected to WIFI
                // network
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isInternetConnected(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm!=null)
        {
            NetworkInfo netinfo = cm.getActiveNetworkInfo();

            if (netinfo != null && netinfo.isConnectedOrConnecting()) {

                return true;
            }
        }

        return false;
    }
}
