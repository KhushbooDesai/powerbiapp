package com.example.a90348353.powerbiapplication.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.a90348353.powerbiapplication.Helper.NetworkManager;
import com.example.a90348353.powerbiapplication.Helper.RetrofitAPIClient;
import com.example.a90348353.powerbiapplication.Helper.SharedPref;
import com.example.a90348353.powerbiapplication.Interface.Api;
import com.example.a90348353.powerbiapplication.Model.LoginResponse;
import com.example.a90348353.powerbiapplication.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private MainActivity activity;
    private EditText etpsno, etpassword;
    private Button btnlogin;
    private CheckBox checkpass;
    private ProgressDialog progressDialog;
    //api inside the interface folder
    private Api api;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //activity = LoginActivity.this;

        sharedPref = new SharedPref(MainActivity.this);
        initView();
        initViewAction();
        initViewListener();


    }

    private void initView() {
        etpsno = findViewById(R.id.etpsno);
        etpassword = findViewById(R.id.etpassword);
        btnlogin = findViewById(R.id.btnlogin);
        checkpass = (CheckBox) findViewById(R.id.checkpass);
    }

    private void initViewAction() {
    }

    private void initViewListener() {
        btnlogin.setOnClickListener(this);
        checkpass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    etpassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnlogin:
                if (NetworkManager.isWifiConnected(MainActivity.this) || NetworkManager.isInternetConnected(MainActivity.this)) {
                    if ((etpsno.getText().toString().equalsIgnoreCase("")) || (etpassword.getText().toString().equalsIgnoreCase(""))) {
                        Toast.makeText(MainActivity.this, "Please Enter PSNO and Password", Toast.LENGTH_SHORT).show();
                    }
                    else if(etpassword.getText().toString().trim().equals(etpsno.getText().toString().trim() + "_PBIAdmin"))
                    {
                        progressDialog = new ProgressDialog(MainActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("Loading..");
                        progressDialog.show();

                        sharedPref.putString("Role", "Admin");
                        sharedPref.putString("userPsno", etpsno.getText().toString().trim());
                        GoToNext();
                    }
                    else {
                        progressDialog = new ProgressDialog(MainActivity.this);
                        progressDialog.setCancelable(false);
                        progressDialog.setMessage("Loading..");
                        progressDialog.show();

                        api = RetrofitAPIClient.getClient().create(Api.class);

                       //logincheck method inside interface
                        Call<LoginResponse> loginResponseCall = api.loginCheck(etpsno.getText().toString(), etpassword.getText().toString());

                        loginResponseCall.enqueue(new Callback<LoginResponse>() {
                            @Override
                            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                                if (response.isSuccessful()) {
                                    //model object
                                    LoginResponse loginResponse = response.body();
                                    switch (loginResponse.getLoginStatus()) {
                                        case "0":
                                            sharedPref.putString("Role", "NormalUser");
                                            sharedPref.putString("userName", loginResponse.getName());

                                                sharedPref.putString("userPsno", etpsno.getText().toString().trim());

                                                sharedPref.putString("userPassword", etpassword.getText().toString().trim());


                                            GoToNext();
                                            break;
                                        case "1":
                                            progressDialogDismiss();
                                            Toast.makeText(MainActivity.this, getString(R.string.Login_WrongPassword), Toast.LENGTH_SHORT).show();
                                            break;
                                        case "2":
                                            progressDialogDismiss();
                                            Toast.makeText(MainActivity.this, getString(R.string.Login_Unauthorized), Toast.LENGTH_SHORT).show();
                                            break;
                                        case "3":
                                            progressDialogDismiss();
                                            Toast.makeText(MainActivity.this, getString(R.string.Login_Blanks), Toast.LENGTH_SHORT).show();
                                            break;
                                        default:
                                            progressDialogDismiss();
                                            Toast.makeText(MainActivity.this, getString(R.string.HttpError), Toast.LENGTH_SHORT).show();
                                            Log.e("loginStatus", String.valueOf(loginResponse.getLoginStatus()));
                                            break;
                                    }
                                } else {
                                    progressDialogDismiss();
                                    Toast.makeText(MainActivity.this, "Response is Unsuccessful", Toast.LENGTH_SHORT).show();
                                    Log.e("response", response.message().toString());
                                }
                            }

                            @Override
                            public void onFailure(Call<LoginResponse> call, Throwable t) {
                                progressDialogDismiss();
                                Toast.makeText(MainActivity.this, "Error While Receiving Response", Toast.LENGTH_SHORT).show();
                                Log.e("Error", t.getMessage());
                            }
                        });
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Please Check your Network Connection", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void progressDialogDismiss() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private void GoToNext() {
        sharedPref.putBoolean("IsFirstTimeLaunch", true);
        Intent intent = new Intent(MainActivity.this, FunctionView.class);
        intent.putExtra("userName", etpsno.getText().toString());
        //intent.putExtra("userPassword",etpassword.getText().toString());
        progressDialogDismiss();
        startActivity(intent);
        finish();
    }

}
