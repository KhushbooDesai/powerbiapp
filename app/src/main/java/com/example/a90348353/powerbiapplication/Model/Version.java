package com.example.a90348353.powerbiapplication.Model;

import com.google.gson.annotations.SerializedName;

public class Version {

    @SerializedName("Res")
    private String Result;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

}
