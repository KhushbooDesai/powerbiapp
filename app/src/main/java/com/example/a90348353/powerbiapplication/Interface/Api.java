package com.example.a90348353.powerbiapplication.Interface;


import com.example.a90348353.powerbiapplication.Model.Functions;
import com.example.a90348353.powerbiapplication.Model.LoginResponse;
import com.example.a90348353.powerbiapplication.Model.Report;
import com.example.a90348353.powerbiapplication.Model.ReportParam;
import com.example.a90348353.powerbiapplication.Model.Version;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api
{

    @GET("LastVisited")
    Call<ReportParam> LastVisited(@Query("PSNO") String psno);

    @GET("GetFunctionData")
    Call<Functions> GetFunctionData(@Query("Psno") String psno);

    @GET("GetReportByFunction")
    Call<Report> GetReportByFunction(@Query("Psno") String psno,@Query("FunctionId") String FunctionID);

    @GET("Frequent")
    Call<Report> Frequent(@Query("PSNO") String psno);

    @GET("InsertReportLog")
    Call<Report> InsertReportLog(@Query("ReportId") String reportId,@Query("Psno") String psno);

    @GET("CheckVersion")
    Call<Version> checkVersion(@Query("CurVersion") String curVersion);


    @FormUrlEncoded
    @POST("Login")
    Call<LoginResponse> loginCheck(@Field("PSNO")String psno, @Field("Password")String Password);
}
